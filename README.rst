ABOUT
=====
This package provide a python library to access the KIT Gruppenverwaltung REST API.


INSTALLATION
============
``$ python setup.py develop``

or

``$ pip install -e .``

or any other method, you use to install your python packages.


USAGE
=====
::

 import kitgvapi
 
 gv = kitgvapi.ObjectInterface()
 gv.config.auth('myusername', 'mypassword')
 
 iism = gv.getOe('IISM')
 for g in iism.get_groups():
     print g.name
 iism.create_group('IISM-EM-newgroup', 'Neue Gruppe')

 newgroup = gv.getGroup('IISM-EM-newgroup')
 newgroup.add_user('zy8373')
 newgroup.add_group('IISM-EM-Alle')
 
 for u in newgroup.get_users(effective=True):
     print "{fn} {ln} ({un})".format(fn=u.firstname, ln=u.lastname, un=u.samaccount)
 for g in newgroup.get_groups():
     print "{gn} ({desc})".format(gn=g.name, desc=g.description)

 me = gv.getUser('zy8373')
 others = me.oe.get_users()


LICENSE
=======
See LICENSE.rst