"""This module contain the ObjectInterface class."""

from .clients import RestClient
from .models import Group, Oe, User


class ObjectInterface(object):
    """Object oriented interface to the KIT Gruppenverwaltung API"""

    class Config(object):
        def __init__(self, provider):
            self.provider = provider

        def auth(self, username, password):
            self.provider.client.config.username = username
            self.provider.client.config.password = password

        def enable_post(self):
            self.provider.client.config.enable_post = True

        def disable_post(self):
            self.provider.client.config.enable_post = False

        def enable_delete(self):
            self.provider.client.config.enable_delete = True

        def disable_delete(self):
            self.provider.client.config.enable_delete = False

    def __init__(self, client=None, client_config=None):
        self.config = self.Config(self)

        if client is None:
            self.client = RestClient(config=client_config)
        else:
            self.client = client

    def getOe(self, pk):
        return Oe(client=self.client, pk=pk)

    def getGroup(self, pk):
        return Group(client=self.client, pk=pk)

    def newGroup(self, name, description):
        raise NotImplementedError("Use kitgvapi.models.Oe.create_group() instead.")

    def getUser(self, pk):
        return User(client=self.client, pk=pk)
