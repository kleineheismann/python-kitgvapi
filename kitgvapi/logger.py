import logging

logging.basicConfig(format='%(levelname)s: %(message)s')
logger = logging.getLogger('kitgvapi')
