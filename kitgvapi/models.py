"""Type library"""

from .clients import RestClient
from .config import SYMBOLS
from .exceptions import MissingClientError, MissingPkError


class Model(object):
    """Abstract base class"""
    def __init__(self, pk=None, client=None, data=None):
        self.pk = pk
        self.client = client
        self.is_loaded = False

        if self.client is None:
            self.client = RestClient()

        if data is not None:
            self._load_data_attribs(data)
            self.is_loaded = True

        if self.pk is not None:
            self.load()

    def _load_data_attribs(self, data=None):
        raise NotImplementedError('Abstract method')

    def load(self, reload=False):
        if self.is_loaded and not reload:
            return
        if self.client is None:
            raise MissingClientError()
        if self.pk is None:
            raise MissingPkError()
        self._load_data_attribs()
        self.is_loaded = True

    def _save_data_attribs(self):
        raise NotImplementedError('Abstract method')

    def save(self):
        if self.client is None:
            raise MissingClientError()
        self._save_data_attribs()


class Group(Model):
    """Represent a KIT group"""
    def _load_data_attribs(self, data=None):
        if data is None:
            data = self.client.get_group(pk=self.pk)
        self.name = data['name']
        self.description = data['description']
        self.gidNumber = data['gidNumber']
        self.readOnly = data['readOnly']
        self.verteiler = data['verteiler']
        self.eMailAddress = data['eMailAddress']
        self.oe_pk = data['oe']
        self.pk = getattr(self, SYMBOLS['GROUP_PK_ATTRIB'])

    def _save_data_attribs(self):
        raise NotImplementedError('Not supported')

    @property
    def oe(self):
        """Hold the Oe object, which this group belongs to (read only)."""
        return Oe(client=self.client, pk=self.oe_pk)

    def get_users(self, effective=False):
        self.load()
        l = self.client.get_group_users(pk=self.pk, effective=effective)
        for i in l:
            y = User(client=self.client, pk=i)
            yield y

    def add_user(self, pk):
        self.client.add_group_user(group_pk=self.pk, user_pk=pk)

    def remove_user(self, pk):
        self.client.remove_group_user(group_pk=self.pk, user_pk=pk)

    def get_groups(self):
        self.load()
        l = self.client.get_group_groups(pk=self.pk)
        for i in l:
            y = Group(client=self.client, pk=i)
            yield y

    def add_group(self, pk):
        self.client.add_group_group(super_pk=self.pk, sub_pk=pk)

    def remove_group(self, pk):
        self.client.remove_group_group(super_pk=self.pk, sub_pk=pk)


class Oe(Model):
    """Represent a KIT OE"""
    def _load_data_attribs(self, data=None):
        if data is None:
            data = self.client.get_oe(pk=self.pk)
        self.kurz = data['kurz']
        self.lang = data['lang']
        self.pk = getattr(self, SYMBOLS['OE_PK_ATTRIB'])

    def _save_data_attribs(self):
        raise NotImplementedError('Not supported')

    def get_groups(self, ad_details=True):
        self.load()
        l = self.client.get_oe_groups(pk=self.pk, ad_details=ad_details)
        for d in l:
            y = Group(client=self.client, data=d)
            yield y

    def get_users(self):
        self.load()
        l = self.client.get_oe_users(pk=self.pk)
        for d in l:
            y = User(client=self.client, data=d)
            yield y

    def create_group(self, name, description):
        self.client.create_group(name, description)

    def delete_group(self, pk):
        self.client.delete_group(pk)


class User(Model):
    """Represent a KIT User account (aka identity)"""
    def _load_data_attribs(self, data=None):
        if data is None:
            data = self.client.get_user(pk=self.pk)
        self.samaccount = data['samaccount']
        self.firstname = data['firstname']
        self.lastname = data['lastname']
        self.oe = Oe(client=self.client, pk=data['oe'])
        self.pk = getattr(self, SYMBOLS['USER_PK_ATTRIB'])

    def _save_data_attribs(self):
        raise NotImplementedError('Not supported')

    def get_groups(self, effective=False):
        self.load()
        l = self.client.get_user_groups(pk=self.pk, effective=effective)
        for d in l:
            y = Group(client=self.client, data=d)
            yield y
