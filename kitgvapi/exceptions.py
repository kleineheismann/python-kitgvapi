"""Exceptions"""


class KitGvApiError(Exception):
    pass


class MissingConfigError(KitGvApiError):
    pass


class MissingClientError(KitGvApiError):
    pass


class MissingPkError(KitGvApiError):
    pass


class NotAllowedError(KitGvApiError):
    pass


class NotFoundError(KitGvApiError):
    pass
