"""(Default) Configuration"""

from .exceptions import MissingConfigError

DEFAULTS = {
    'URL_SCHEMA': 'https://',
    'SERVER': 'kit-idm-03.scc.kit.edu',
    'BASE_URI': '/itbportal-rest-war/rest',
    'USERNAME': MissingConfigError('No username set'),
    'PASSWORD': MissingConfigError('No password set'),
    'ENABLE_POST': True,
    'ENABLE_DELETE': False,
}

SYMBOLS = {
    'GROUP_PK_ATTRIB': 'name',
    'OE_PK_ATTRIB': 'kurz',
    'USER_PK_ATTRIB': 'samaccount',
}
