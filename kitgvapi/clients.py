"""Clients/Consumer for the remote API provider"""

import json
import requests

from .config import DEFAULTS, SYMBOLS
from .exceptions import NotAllowedError, NotFoundError, MissingConfigError


class RestClientConfig(object):
    """Config for RestClient.
    
    An instance of this class is used to provide the necessary
    configuration parameters to a kitgvapi.clients.RestClient
    instance.
    It takes its default values from kitgvapi.config.DEFAULTS.
    """

    def __init__(self):
        self.url_schema = DEFAULTS['URL_SCHEMA']
        self.server = DEFAULTS['SERVER']
        self.base_uri = DEFAULTS['BASE_URI']

        self._username = DEFAULTS['USERNAME']
        self._password = DEFAULTS['PASSWORD']
        self._credentials = None

        self.enable_post = DEFAULTS['ENABLE_POST']
        self.enable_delete = DEFAULTS['ENABLE_DELETE']

    @property
    def username(self):
        if isinstance(self._username, MissingConfigError):
            raise self._username
        return self._username

    @username.setter
    def username(self, username):
        self._username = username
        self._credentials = None

    @property
    def password(self):
        if isinstance(self._password, MissingConfigError):
            raise self._password
        return self._password

    @password.setter
    def password(self, password):
        self._password = password
        self._credentials = None

    @property
    def credentials(self):
        """Setup and return a requests.auth.HTTPBasicAuth instance."""
        if self._credentials is None:
            u = self.username
            if u:
                self._credentials = requests.auth.HTTPBasicAuth(u, self.password)
        return self._credentials


class RestClient(object):
    """Send requests to the REST API.

    - the get_* methods send GET requests and return the
      responded and already deserialized JSON data.
      Methods, which names end with 's' return a list.
    - the add_* and create_* methods send POST requests.
    - the remove_* and delete_* methods send DELETE requests.
    """
    def __init__(self, config=None):
        """:param config: may be an instance of kitgvapi.clients.RestClientConfig. If omitted, a default config is loaded.
        """
        self.config = config
        if self.config is None:
            self.config = RestClientConfig()

    def get(self, path):
        """Send GET request."""
        url = self.config.url_schema + self.config.server + self.config.base_uri + path
        try:
            r = requests.get(url, verify=True, auth=self.config.credentials)
            r.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 404:
                raise NotFoundError("Endpoint returned Not Found")
            else:
                raise e
        d = r.json()
        return d

    def post(self, path, data=None):
        """Send POST request."""
        if not self.config.enable_post:
            raise NotAllowedError("POST operations are disabled by configuration")
        url = self.config.url_schema + self.config.server + self.config.base_uri + path
        if data is not None:
            data = json.dumps(data)
        headers = {'content-type': 'application/json'}
        r = requests.post(url, verify=True, auth=self.config.credentials, data=data, headers=headers)
        r.raise_for_status()
        return r

    def delete(self, path):
        """Send DELETE request."""
        if not self.config.enable_delete:
            raise NotAllowedError("DELETE operations are disabled by configuration")
        url = self.config.url_schema + self.config.server + self.config.base_uri + path
        r = requests.delete(url, verify=True, auth=self.config.credentials)
        r.raise_for_status()
        return r

    def get_oes(self, pk_only=False):
        path = '/oe/list'
        l = self.get(path)
        if pk_only:
            l = [d[SYMBOLS['OE_PK_ATTRIB']] for d in l]
        return l

    def get_oe(self, pk):
        path = '/oe/name/{pk}'.format(pk=pk)
        return self.get(path)

    def get_oe_groups(self, pk, pk_only=False, ad_details=True):
        path = '/groups/{pk}'.format(pk=pk)
        if ad_details:
            path += "?addAdDetails=true"
        l = self.get(path)
        if pk_only:
            l = [d[SYMBOLS['GROUP_PK_ATTRIB']] for d in l]
        return l

    def get_oe_users(self, pk, pk_only=False):
        path = '/oe/name/{pk}/identities'.format(pk=pk)
        l = self.get(path)
        if pk_only:
            l = [d[SYMBOLS['USER_PK_ATTRIB']] for d in l]
        return l

    def get_user(self, pk):
        path = '/identities/get/{pk}'.format(pk=pk)
        return self.get(path)

    def get_user_groups(self, pk, pk_only=False, effective=False):
        path = '/identities/get/{pk}/memberof'.format(pk=pk)
        if effective:
            res = 'all'
        else:
            res = 'direct'
        l = self.get(path)[res]
        if pk_only:
            l = [d[SYMBOLS['GROUP_PK_ATTRIB']] for d in l]
        return l

    def get_group(self, pk):
        group_pk = pk
        oe_pk = pk.partition('-')[0]
        path = '/groups/{oe}/{group}'.format(oe=oe_pk, group=group_pk)
        return self.get(path)

    def get_group_users(self, pk, pk_only=True, effective=False):
        if not pk_only:
            raise NotImplemented('Not supported')
        group_pk = pk
        oe_pk = pk.partition('-')[0]
        res = 'members'
        if effective:
            res = 'effective' + res
        path = '/groups/{oe}/{group}/{res}'.format(oe=oe_pk, group=group_pk, res=res)
        return self.get(path)

    def get_group_groups(self, pk, pk_only=True):
        if not pk_only:
            raise NotImplemented('Not supported')
        group_pk = pk
        oe_pk = pk.partition('-')[0]
        path = '/groups/{oe}/{group}/subgroups'.format(oe=oe_pk, group=group_pk)
        return self.get(path)

    def add_group_user(self, group_pk, user_pk):
        oe_pk = group_pk.partition('-')[0]
        path = '/groups/{oe}/{group}/members/{user}'.format(oe=oe_pk, group=group_pk, user=user_pk)
        self.post(path)

    def remove_group_user(self, group_pk, user_pk):
        oe_pk = group_pk.partition('-')[0]
        path = '/groups/{oe}/{group}/members/{user}'.format(oe=oe_pk, group=group_pk, user=user_pk)
        self.delete(path)

    def add_group_group(self, super_pk, sub_pk):
        oe_pk = super_pk.partition('-')[0]
        path = '/groups/{oe}/{superg}/subgroups/{subg}'.format(oe=oe_pk, superg=super_pk, subg=sub_pk)
        self.post(path)

    def remove_group_group(self, super_pk, sub_pk):
        oe_pk = super_pk.partition('-')[0]
        path = '/groups/{oe}/{superg}/subgroups/{subg}'.format(oe=oe_pk, superg=super_pk, subg=sub_pk)
        self.delete(path)

    def create_group(self, name, description):
        oe_pk = name.partition('-')[0]
        path = '/groups/{oe}'.format(oe=oe_pk)
        data = {'name': name, 'description': description}
        self.post(path, data=data)

    def delete_group(self, pk):
        oe_pk = pk.partition('-')[0]
        path = '/groups/{oe}/{group}'.format(oe=oe_pk, group=pk)
        self.delete(path)
