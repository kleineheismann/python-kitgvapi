"""
Library for accessing the KIT Gruppenverwaltung REST API

The object, you are looking for is kitgvapi.ObjectInterface.
"""

from .object_interface import ObjectInterface
from .clients import RestClient
from .models import Group, Oe, User
