#!/usr/bin/env python
from setuptools import setup, find_packages


def get_readme():
    with open('README.rst') as f:
        return f.read()


def get_license():
    with open('LICENSE.rst') as f:
        return f.read()


setup(
    name='python-kitgvapi',
    version='1.3.2',
    description='Library for accessing the KIT Gruppenverwaltung REST API',
    long_description=get_readme(),
    author='Jens Kleineheismann',
    author_email='kleineheismann@kit.edu',
    license=get_license(),
    url='https://git.scc.kit.edu/zy8373/python-kitgvapi',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'requests',
    ],
)
